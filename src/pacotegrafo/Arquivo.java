package pacotegrafo;

import java.io.File;

import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.PrintWriter;

import java.io.IOException;
import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class Arquivo {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private static String linha;
    private static String informacoes;
    
    private static File f;
    
    private static FileReader fr;
    private static BufferedReader br;
    
    private static FileWriter fw;
    private static PrintWriter saida;
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para ler o arquivo de entrada de dados.
    public static String lerArquivo(String _caminhoLeitura){
        
        informacoes = "";
        
        try{
            
            // Associando o arquivo físico com o atributo lógico 'fr'.
            // A partir de agora a manipulação do arquivo deve ser feita
            // utilizando 'fr'.
            FileReader fr = new FileReader(_caminhoLeitura);
            BufferedReader br = new BufferedReader(fr); // Associar a entrada de dados com 'fr'.
            
            linha = br.readLine(); // Faz a leitura da primeira linha do arquivo texto.
            
            // Enquanto não chegar ao final do arquivo.
            while (linha != null){
                
                // Separando os itens de cada linha.
                informacoes += linha;
                linha = br.readLine(); // Avança para a próxima linha do arquivo texto.
                
            }
            
        }catch (FileNotFoundException fne){
            JOptionPane.showMessageDialog(null, "\"" + _caminhoLeitura + "\" não existe.");
        }catch (IOException ioe){
            JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo.");
        }finally{

            try {
                if (br != null) br.close(); // Fecha o buffer
                if (fr != null) fr.close(); // Fecha o arquivo
            } catch (IOException ex) {
            
            }
            
        }
        
        return informacoes;
        
    }
    
    // Método base para gerar o relatório de saídade de dados.
    public static void gerarRelatorio(String _caminhoEscrita, String _texto){
        
        try{
            
            f = new File(_caminhoEscrita);
            
            // Caso o arquivo não existir,
            if (!f.exists())
                f.createNewFile(); // Cria o arquivo texto.
            
            // Associando o arquivo físico com o atributo lógico 'fw'.
            // A partir de agora a manipulação do arquivo deve ser feita
            // utilizando 'fw'.
            fw = new FileWriter (f);
            saida = new PrintWriter (fw); // Associar a saída de dados com 'fw'.
            
            saida.println(_texto); // Escrevendo no arquivo texto.
            
        }catch (IOException ioe){
        
            JOptionPane.showMessageDialog(null, "Erro ao escrever arquivo.");

        }finally{
            
            try {
                if (saida != null) saida.close(); // Fecha a ligação com 'fw'.
                if (fw != null) fw.close();       // Fecha o arquivo
            } catch (IOException ex) {
            
            }
            
        }
        
    }
    
}
