package pacotegrafo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class Grafo {

    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    private final String arquivoMA = ".\\src\\arquivos\\MA.txt";
    private final String arquivoMD = ".\\src\\arquivos\\MD.txt";
    
    String tituloDasTelas = "Grafo";

    private String dadosDoArquivo;
    
    private int MatrizAdjacencia[][];
    private int MatrizDistancias[][];
    private String auxMatrizDistancias[];
    
    private int qtdVertices;
    
    public String textoe="";
    
    private NoVetor[] listaDeAdjacencia;
    
    private String[] Vertices;

 
    /* ====================================
            MÉTODO CONSTRUTOR DA CLASSE
       ==================================== */
    
    public Grafo(){
    
        this.setQtdVertices(0);
        
    
    }
    
    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */

    public String[] getVertices() {
        return Vertices;
    }

    public void setVertices(String[] Vertices) {
        this.Vertices = Vertices;
    }
    
    
    public String getDadosDoArquivo() {
        return this.dadosDoArquivo;
    }

    public void setDadosDoArquivo(String _dDA) {
        this.dadosDoArquivo = _dDA;
    }
    
    public int[][] getMatrizDeAdjacencia() {
        return this.MatrizAdjacencia;
    }

    public void setMatrizDeAdjacencia(int[][] _mA) {
        this.MatrizAdjacencia = _mA;
    }

    public int[][] getMatrizDeDistancias() {
        return this.MatrizDistancias;
    }

    public void setMatrizDeDistancias(int[][] _mD) {
        this.MatrizDistancias = _mD;
    }
    
    public int getQtdVertices() {
        return this.qtdVertices;
    }

    public void setQtdVertices(int _qV) {
        this.qtdVertices = _qV;
    }

    public NoVetor[] getListaDeAdjacencia() {
        return this.listaDeAdjacencia;
    }

    public void setListaDeAdjacencia(NoVetor[] _lA) {
        this.listaDeAdjacencia = _lA;
    }
    
    /* ====================================
               OPERAÇÕES DA CLASSE
       ==================================== */
    
    // Método para ler o arquivo de entrada de dados.
    private void lerArquivo(String _arquivo){
        
        this.dadosDoArquivo = Arquivo.lerArquivo(_arquivo);
                
    }
    
    // Método para criar um arquivo do tipo texto.
    private void criarArquivo(){
        
        
        
    }
    
    // Método para criar a matriz de adjacência.
    public void criarMA(){
        
        this.lerArquivo(this.arquivoMA);
        
        this.setQtdVertices(Integer.parseInt(String.valueOf(this.getDadosDoArquivo().charAt(0))));
        
        this.setMatrizDeAdjacencia(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
        int p = 1;
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            for (int C = 0; C < this.getQtdVertices(); C++){
                
                this.getMatrizDeAdjacencia()[L][C] = Integer.parseInt(String.valueOf(this.getDadosDoArquivo().charAt(p)));
                p++;
                System.out.println("Matriz ["+L+"],["+C+"]="+this.getMatrizDeAdjacencia()[L][C]);
            }
        
        }
        
    }
    
    // Método para resgatar o valor de um determinado quadrante da matriz.
    // _VO -> Vértice origem
    // _VD -> Vértice destino
    private int resgatarQuadrante(int _VO, int _VD, int opcao){
        
        return opcao == 1 ? this.getMatrizDeAdjacencia()[_VO][_VD] : this.getMatrizDeDistancias()[_VO][_VD];
        
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMA(){
        
        String texto = "\n";
        
        texto += "Matriz de adjacência do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
           texto += this.getListaDeAdjacencia()[L].getVertice()+ " ";
            //texto += L + " ";
        
        texto += "\n";
        
        
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            texto += this.getListaDeAdjacencia()[L].getVertice()+ " ";
            //texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,1) + " ";
            
            texto += "\n";
            
        }
        
        this.textoe+=texto;
       
        
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMD(){
        
        String texto = "\n";
        
        texto += "Matriz de distâncias do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
            texto += L + " ";
        
        texto += "\n";
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,2) + " ";
            
            texto += "\n";
            
        }
        this.textoe+=texto;
       
        
    }
    
    private void inicializarLA(){
        
        // Verificando se já foi criada a matriz de adjacência.     
        if(this.getQtdVertices() > 0){
            
            // Lendo o arquivo matriz de distâncas.
            this.lerArquivo(this.arquivoMD);

            // Instanciando a matriz de distâncias
            this.setMatrizDeDistancias(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
            // Criando uma instância do vetor.
            this.setListaDeAdjacencia(new NoVetor[this.getQtdVertices()]);
            
            // Começando a contagem a partir da quantidade de vértices do grafo.
            int p = this.getQtdVertices()+1;

            // Preenchendo a matriz de distâncias
            for (int L = 0; L < this.getQtdVertices(); L++){

                for (int C = 0; C < this.getQtdVertices(); C++){

                    this.getMatrizDeDistancias()[L][C] = Integer.parseInt(String.valueOf(this.getDadosDoArquivo().charAt(p)));
                    p++;

                }

            }
            
            this.Vertices = new String[this.getQtdVertices()];
            
            // Inicializando as listas encadeadas para cada posição do vetor
            for(int i = 0; i < this.getQtdVertices(); i++){
            
                this.Vertices[i] = String.valueOf(this.getDadosDoArquivo().charAt(i+1));
            
                this.getListaDeAdjacencia()[i] = new NoVetor();
                
                this.getListaDeAdjacencia()[i].iniciarPosicaoVetor();
                this.getListaDeAdjacencia()[i].setVertice(this.getDadosDoArquivo().charAt(i+1));
                
            }
            
        }
        
    }
    
    // Método para criar a lista de adjacência.
    public void criarLA(){
        
        this.inicializarLA();
        
        for (int L = 0; L < this.getQtdVertices(); L++){

            for (int C = 0; C < this.getQtdVertices(); C++){
                
                if (this.getMatrizDeDistancias()[L][C] != 0){
                    
                    No novoNo = new No();
                    
                    novoNo.setValorV(this.Vertices[C]);
                    novoNo.setDistancia(this.getMatrizDeDistancias()[L][C]);
                    
                    if(this.getListaDeAdjacencia()[L].getQuantidadeDeNos() == 0){
                        
                        this.getListaDeAdjacencia()[L].setPrimeiroNo(novoNo);
                        
                    }else{
                        
                        this.getListaDeAdjacencia()[L].getUltimoNo().setProximo(novoNo);
                        
                    }
                    
                    this.getListaDeAdjacencia()[L].setUltimoNo(novoNo);
                    this.getListaDeAdjacencia()[L].setQuantidadeDeNos(this.getListaDeAdjacencia()[L].getQuantidadeDeNos()+1);
                    
                }
                
            }

        }
        
    }
    
    // Método para imprimir a lista de adjacência.
    public void imprimirLA(){
        
        String texto = "";
        No aux;
        
        texto += "Lista de adjacência do Gráfo G:\n\n";
        
        for (int i = 0; i < this.getQtdVertices(); i++){
            
            texto += this.getListaDeAdjacencia()[i].getVertice() + " : ";
            
            aux = this.getListaDeAdjacencia()[i].getPrimeiroNo();
            
            while(aux != null){
                
                texto += aux.getValorV() + " -> ";
                aux = aux.getProximo();
                
            }
            
            texto += "null\n";
            
        }
        
       
        
    }
    
    // Método para registrar null ao objeto G.
    public void sair(){
        
        // Inicializando as listas encadeadas para cada posição do vetor
        for(int i = 0; i < this.getQtdVertices(); i++){

            this.getListaDeAdjacencia()[i] = null;

            this.getListaDeAdjacencia()[i].iniciarPosicaoVetor();
            this.getListaDeAdjacencia()[i].setVertice(this.getDadosDoArquivo().charAt(i+1));

        }
        
      
        
    }
    
    public void qtdvertice(){
    
}
        public void imprimirLista(int x)
        {
          
             
        String texto = "\n";
        No aux;
        
        texto += "Lista de adjacência do Gráfo G:\n\n";
        
        for (int i = 0; i < this.getQtdVertices(); i++){
            
            texto += this.getListaDeAdjacencia()[i].getVertice() + " : ";
            
            aux = this.getListaDeAdjacencia()[i].getPrimeiroNo();
            
            while(aux != null){
                
                texto += aux.getValorV() + " -> ";
                aux = aux.getProximo();
                
            }
            
            texto += "null\n";
            
        }
        this.textoe+=texto;
        
        
        
          
         }
        // Conta o grau de cada vertice
        public void grau()
        {
            String texto = "\n\n";
          for (int L = 0; L < this.getQtdVertices(); L++){
              texto += this.getListaDeAdjacencia()[L].getVertice()+ " - ";
              int contGrau=0;
                     
            for (int C = 0; C < this.getQtdVertices(); C++)
            {
                
                if(this.resgatarQuadrante(L,C,1)==1)
                    contGrau++;
            }  
            texto+= contGrau +"\n"; 
            
        }
          this.textoe+="Graus:\n"+texto+"\n";
         
        }
        
        
        //Quantidade de loop
         public int Loop(){
        
        String texto = "";
        
        texto += "loop G:\n\n";
        texto += "   ";
        int contLoop=0;
              
        for (int L = 0; L < this.getQtdVertices(); L++){
            
                     
            for (int C = 0; C < this.getQtdVertices(); C++)
            {
                if(this.resgatarQuadrante(L,C,1)!=0&&L==C)
                    contLoop++;              
            }
        }
        texto += contLoop;
        this.textoe+="Qtd Loop = "+contLoop+"\n";
      
        return contLoop;
        
    }
         
         // Lista os vizinhos do vertice
         public void Vizinhança(){
              String texto = "";
        No aux;
        
        texto += "Vizinhança\n\n";
        
        for (int i = 0; i < this.getQtdVertices(); i++){
            
            texto += this.getListaDeAdjacencia()[i].getVertice() + " - ";
            
            aux = this.getListaDeAdjacencia()[i].getPrimeiroNo();
            
            while(aux != null){
                if(aux.getValorV()!=null)
                texto += aux.getValorV() + "  ";
                aux = aux.getProximo();
                
            }
            
            texto += "\n";
            
        }
        this.textoe+=texto+"\n\n";
         }
         
         // BUSCA QUANTIDADE DE ARESTA
         public int qtdAresta()
         {
             int contAreta=0;
              
        for (int L = 0; L < this.getQtdVertices(); L++){
            
                     
            for (int C = L; C < this.getQtdVertices(); C++)
            {
                  if(this.resgatarQuadrante(L,C,1)!=0)
                    contAreta++;              
             }
                
         }
        this.textoe+="Qtd Aresta = "+contAreta+"\n";
        return contAreta;
         }
         
         public void ListarAresta()
         {
             this.textoe+="\nV = {";
             for (int i = 0; i < this.getQtdVertices(); i++)
            {
                    this.textoe+=this.getListaDeAdjacencia()[i].getVertice();
                    if (i<(this.getQtdVertices()-1))
                        this.textoe+=" , ";
             }
             this.textoe+="}\n\n";
         }
         
         // Vrifica s existe mais familias
         public void familia() {
        int qtdFamilia = 0;

        for (int L = 0; L < this.getQtdVertices(); L++) {
            for (int C = L; C < this.getQtdVertices(); C++) {
                if (MatrizAdjacencia[L][C] == 1) {
                    //verifica todas as linha e coluna da respcetiva celula

                    //verficando na linha
                    int qtdNaLinha = 0;
                    for (int i = 0; i < getQtdVertices(); i++) {
                        if (MatrizAdjacencia[L][i] == 1) {
                            qtdNaLinha++;
                        }
                    }

                    //verficando na coluna
                    int qtdNaColuna = 0;
                    for (int i = 0; i < getQtdVertices(); i++) {
                        if (MatrizAdjacencia[i][C] == 1) {
                            qtdNaColuna++;
                        }
                    }

                    if (qtdNaLinha == 1 && qtdNaColuna == 1) {
                        qtdFamilia++;
                    }

                }
            }
        }
        if(qtdFamilia==0)
            this.textoe+="FAMILIA: Nenhuma \n\n";
        else
            this.textoe+="FAMILIA: "+qtdFamilia+"\n\n";
       
    }
         // ligação dos vertices "E"
        public void ERelaciona()
         {
             String texto="";
              
        for (int L = 0; L < this.getQtdVertices(); L++){
            
                     
            for (int C = L; C < this.getQtdVertices(); C++)
            {
                  if(this.resgatarQuadrante(L,C,1)!=0)
                  {
                    texto+=" "+  this.getListaDeAdjacencia()[L].getVertice() + " "+this.getListaDeAdjacencia()[C].getVertice()+" , ";  
                  
                     
                  }
             }
                
         }
        texto = texto.substring (0, texto.length() - 3);
        this.textoe+="E = { "+texto+" }";
       
        
         }
        
        // Classificação do grafo;
        public void ClassGrafo()
        {
            String texto="";
            int aux= 0;
            aux= this.qtdAresta();
            if(this.qtdAresta()==0)
                texto="Vazio";
            else if((this.qtdAresta()/2)==this.getQtdVertices())
            texto="Simples";
            else
            texto="MultiGrafo,";
            
            
            this.textoe+="Grafo = "+texto+"\n";
        }
        
        //lista adjacencia
                public void Listaadj()
         {
             String texto="";
              
        for (int L = 0; L < this.getQtdVertices(); L++){
            texto+="\n"+ this.getListaDeAdjacencia()[L].getVertice()+" - ";
                     
            for (int C = 0; C < this.getQtdVertices(); C++)
            {
                  if(this.resgatarQuadrante(L,C,1)!=0)
                  {
                      if(L<=C)
                    texto+= " "+  this.getListaDeAdjacencia()[L].getVertice() + ""+this.getListaDeAdjacencia()[C].getVertice() + " ,"; 
                      else texto+= " "+  this.getListaDeAdjacencia()[C].getVertice() + ""+this.getListaDeAdjacencia()[L].getVertice() + " ,";
             }
                texto+="";
         }}
        this.textoe+="\n\nLista Adjacente\n"+texto+"\n";
        
        
         }
         
        
         public void menorCaminho(String _x, String _y)
         {
             int _z=0,_u=0;
             String Aux;
             for(int i=0;i<getQtdVertices();i++)
             {
                 Aux=this.getVertices()[i];
                 if(_x.equals(Aux))
                    _z=i;
              if(_y.equals(Aux))
                    _u=i;
             }
             System.out.println("_z:"+_z+"  _u:"+_u);
             //menorCaminhoP(_u,_z);
         }
         
         public void GravarArquico() throws IOException {
             
         File file = new File(".\\src\\arquivos\\SALVE.txt");
         file.delete();
         FileWriter arq = new FileWriter(".\\src\\arquivos\\SALVE.txt");
        PrintWriter gravarArq = new PrintWriter(arq);
        gravarArq.println(this.textoe);
        arq.close();
          }
      //   public void menorCaminhoP(grafo anterior,No d,No v,int v,int z){
             
         
        //     System.out.println("Teste");
             
        // }
        
        
 /*
         // Atributos usados na funcao encontrarMenorCaminho

    NoVetor menorCaminho = new NoVetor(null, 0, 0);

    // Variavel que recebe os vértices pertencentes ao menor caminho
    NoVetor verticeCaminho = new NoVetor(null, 0, 0);

    // Variavel que guarda o vértice que esta sendo visitado
    NoVetor atual = new NoVetor(null, 0, 0);

    // Variavel que marca o vizinho do vértice atualmente visitado
    NoVetor vizinho = new NoVetor(null, 0, 0);

    // Lista dos vertices que ainda nao foram visitados
    List<NoVetor> naoVisitados = new ArrayList<NoVetor>();

    // Algoritmo de Dijkstra
    public List<NoVetor> encontrarMenorCaminhoDijkstra(Grafo grafo, NoVetor v1,
            NoVetor v2) {

        // Adiciona a origem na lista do menor caminho
        menorCaminho.getId();

        // Colocando a distâncias iniciais
        for (Grafo ) {

            // Vértice atual tem distância zero, e todos os outros,
            // ("infinita - qualquer valor")
            if (grafo.getVertice().get(i).getDescricao()
                    .equals(v1.getDescricao())) {

                grafo.getVertices().get(i).setDistancia(0);

            } else {

                grafo.getVertices().get(i).setDistancia(9999);

            }
            // Insere o vertice na lista de vertices nao visitados
            this.naoVisitados.add(grafo.getVertices().get(i));
        }

        Collections.sort(naoVisitados);

        // O algoritmo continua ate que todos os vertices sejam visitados
        while (!this.naoVisitados.isEmpty()) {

            // Toma-se sempre o vertice com menor distancia, que eh o primeiro
            // da
            // lista

            atual = this.naoVisitados.get(0);
            System.out.println("Pegou esse vertice:  " + atual);
            /*
             * Para cada vizinho (cada aresta), calcula-se a sua possivel
             * distancia, somando a distancia do vertice atual com a da aresta
             * correspondente. Se essa distancia for menor que a distancia do
             * vizinho, esta eh atualizada.
             *
            for (int i = 0; i < atual.getArestas().size(); i++) {

                vizinho = atual.getArestas().get(i).getDestino();
                System.out.println("Olhando o vizinho de " + atual + ": "
                        + vizinho);
                if (!vizinho.verificarVisita()) {

                    // Comparando a distância do vizinho com a possível
                    // distância
                    if (vizinho.getDistancia() > (atual.getDistancia() + atual
                            .getArestas().get(i).getPeso())) {

                        vizinho.setDistancia(atual.getDistancia()
                                + atual.getArestas().get(i).getPeso());
                        vizinho.setPai(atual);

                        /*
                         * Se o vizinho eh o vertice procurado, e foi feita uma
                         * mudanca na distancia, a lista com o menor caminho
                         * anterior eh apagada, pois existe um caminho menor
                         * vertices pais, ateh o vertice origem.
                         *
                        if (vizinho == v2) {
                            menorCaminho.clear();
                            verticeCaminho = vizinho;
                            menorCaminho.addAdj(vizinho);
                            while (verticeCaminho.getPai() != null) {

                                menorCaminho.add(verticeCaminho.getPai());
                                verticeCaminho = verticeCaminho.getPai();

                            }
                            // Ordena a lista do menor caminho, para que ele
                            // seja exibido da origem ao destino.
                            Collections.sort(menorCaminho);

                        }
                    }
                }

            }
            // Marca o vertice atual como visitado e o retira da lista de nao
            // visitados
            atual.visitar();
            this.naoVisitados.remove(atual);
            /*
             * Ordena a lista, para que o vertice com menor distancia fique na
             * primeira posicao
             *

            Collections.sort(naoVisitados);
            System.out.println("Nao foram visitados ainda:"+naoVisitados);

        }

        return menorCaminho;
    }
*/
}
