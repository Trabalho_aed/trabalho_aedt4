package pacotegrafo;

import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Marcio Aguiar, Kassiane Silva e Wilderson Rosa ,Arlindo , Leone e Turma
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        /* ====================================
                   ATRIBUTOS DA CLASSE
           ==================================== */
        
        int opcao; // Opção para trabalhar o menu principal.
        
        String tituloDasTelas = "Trabalho 4";
        
        /* ====================================
             ATRIBUTOS DE INSTÂNCIA DA CLASSE
           ==================================== */
        
        Grafo G = new Grafo();
        
        /* ====================================
                   OPERAÇÕES DA CLASSE
           ==================================== */
        
        do{
            
            opcao = menuPrincipal();
            
            switch(opcao){
                
                case 0: G.sair();
                        JOptionPane.showMessageDialog(null,"Tchau...",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        break;
                
               
                case 1: G.criarMA();
                         G.criarLA();
                         G.ListarAresta();
                         G.ERelaciona();


                         G.Listaadj();

                         G.getQtdVertices();
                         G.qtdAresta();
                         G.Loop();
                         G.ClassGrafo();
                         G.familia();

                         G.Vizinhança();
                         G.grau();
                         G.GravarArquico();
                        break;
                        
                default: JOptionPane.showMessageDialog(null,"Opção inválida!","Advertência",JOptionPane.WARNING_MESSAGE);
                
            }
            
        }while(opcao != 0);
        
    }
    
    // Método para acessar o menu principal da aplicação.
    private static int menuPrincipal(){
        
        return Integer.parseInt(JOptionPane.showInputDialog(null, 
                                                                  "=========================\n"
                                                                + " 0 - Sair            \n"
                                                                + " 1 - Ler e Gravar Dados Grafo\n\n",
                                                                     
                                                                "Menu principal", JOptionPane.QUESTION_MESSAGE));
        
    }
        
}